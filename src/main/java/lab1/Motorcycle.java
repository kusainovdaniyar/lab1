package lab1;

import lab1.exceptions.DuplicateModelNameException;
import lab1.exceptions.ModelPriceOutOfBoundsException;
import lab1.exceptions.NoSuchModelNameException;

public class Motorcycle implements Transport {


    private Model head = new Model();

    {
        head.prev = head;
        head.next = head;
    }

    private int size = 0;
    private String brand;

    public Motorcycle(String brand, int size) {
        this.brand = brand;
        for (int i = 0; i < size; i++) {
            try {
                addModel("nameModel" + i, 5 + i);
            } catch (DuplicateModelNameException e) {
                e.printStackTrace();
            }
        }
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String[] getModelsName() {
        String[] modelName = new String[size];
        Model first = head.getNext();
        int i = 0;
        while (first != head) {
            modelName[i++] = first.getModelName();
            first = first.getNext();
        }
        return modelName;
    }

    public double getPriceByName(String name) throws NoSuchModelNameException {
        Model first = head.getNext();
        while (first != head) {
            if (first.getModelName().equals(name)) {
                return first.getPrice();
            }
            first = first.getNext();
        }
        throw new NoSuchModelNameException();
    }

    public void setPriceByName(String name, double changePrice) throws NoSuchModelNameException {
        if (changePrice < 0) {
            throw new ModelPriceOutOfBoundsException();
        }
        Model first = head.getNext();
        while (first != head) {
            if (first.getModelName().equals(name)) {
                first.setPrice(changePrice);
                return;
            }
            first = first.getNext();
        }
        throw new NoSuchModelNameException();
    }

    public double[] getPrices() {
        double[] prices = new double[size];
        Model first = head.getNext();
        int i = 0;
        while (first != head) {
            prices[i++] = first.getPrice();
            first = first.getNext();
        }
        return prices;
    }

    public void addModel(String nameModel, double price) throws DuplicateModelNameException {
        Model first = head.getNext();
        while (first != head) {
            if (first.getModelName().equals(nameModel)) {
                throw new DuplicateModelNameException();
            }
            first = first.getNext();
        }
        if (price < 0) {
            throw new ModelPriceOutOfBoundsException();
        }
        Model model = new Model(nameModel, price);
        model.prev = head.prev;
        head.prev.next = model;
        head.prev = model;
        model.next = head;
        size++;
    }

    public void removeModel(String nameModel, double price) throws NoSuchModelNameException {
        if (price < 0) {
            throw new ModelPriceOutOfBoundsException();
        }
        Model first = head.getNext();
        Model deleteModel = null;
        while (first != head) {
            if (first.getModelName().equals(nameModel)) {
                deleteModel = first;
                deleteModel.prev.next = deleteModel.next;
                deleteModel.next.prev = deleteModel.prev;
                size--;
                return;
            }
            first = first.getNext();
        }
        throw new NoSuchModelNameException();
    }

    public int getSizeModels() {
        return size;
    }

    public Motorcycle clone() throws CloneNotSupportedException {
        Motorcycle clone = (Motorcycle) super.clone();
        clone.size = 0;          //чтобы при добавлении список не был х2
        clone.head = new Model();//Создаём новую голову
        clone.head.next = clone.head;
        clone.head.prev = clone.head;
        Model target = head.next;
        try {
            while (target != head) {
                clone.addModel(target.getModelName(), target.getPrice());
                target = target.next;
            }
        } catch (DuplicateModelNameException e) {
            e.printStackTrace();
        }
        return clone;
    }

    public void setModelByName(String name, String changeOn) throws NoSuchModelNameException, DuplicateModelNameException {
        Model first = head.getNext();
        String[] models = getModelsName();
        for (int i = 0; i <models.length ; i++) {
            if(models[i].equals(changeOn)){
                throw new DuplicateModelNameException();
            }
        }
        while (first != head) {
            if (first.getModelName().equals(name)) {
                first.setModelName(changeOn);
                return;
            }
            first = first.getNext();
        }
        throw new NoSuchModelNameException();
    }

    private class Model {
        String modelName = null;
        double price = Double.NaN;
        Model prev = null;
        Model next = null;

        public Model() {
        }


        public Model(String modelName, double price) {
            this.modelName = modelName;
            this.price = price;
        }

        public String getModelName() {
            return modelName;
        }

        public void setModelName(String modelName) {
            this.modelName = modelName;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public Model getPrev() {
            return prev;
        }

        public void setPrev(Model prev) {
            this.prev = prev;
        }

        public Model getNext() {
            return next;
        }

        public void setNext(Model next) {
            this.next = next;
        }
    }


}
