package lab1;

import lab1.exceptions.DuplicateModelNameException;
import lab1.exceptions.NoSuchModelNameException;

public class Main {
    public static void main(String[] args) throws CloneNotSupportedException {


//        Properties propertyLoading = lab1.PropertyLoading.getInstance().getProperties();
//        System.out.println(propertyLoading.toString());
//
//        Properties propertyLoading1 = lab1.PropertyLoading.getInstance().getProperties();
//        System.out.println(propertyLoading1.toString());
//

        //////////////////////////////////////////////////////////////////////////////


        Transport car = JobWithTransport.createInstance("Опель", 4);

        try {
            car.addModel("Астра", 10);
            System.out.println("цена " + car.getPriceByName("nameModel1"));
            JobWithTransport.printModels(car);
            car.setModelByName("nameModel0", "АстраГТ");
            System.out.println("изменнное");
            car.setModelByName("nameModel1", "nameModel3");
            JobWithTransport.printModels(car);
            // car.addModel("Астра", -1.0); //для исключения
            // car.addModel("nameModel0", 1.0);
            car.removeModel("Астра", 10);
            System.out.println("Удаляем модель:");
            JobWithTransport.printModels(car);
            System.out.println("Размер " + car.getSizeModels());

        } catch (DuplicateModelNameException | NoSuchModelNameException e) {
            System.out.println(e.getMessage());
        }
        //ПРОВЕРКА КЛОНА
        Car carClone = (Car) car.clone();
        try {
            carClone.setPriceByName("nameModel1", 10.0);
            JobWithTransport.printModels(carClone);
            JobWithTransport.printModels(car);
        } catch (NoSuchModelNameException e) {
            e.printStackTrace();
        }


        JobWithTransport.setTransportFactory(new MotorcycleFactory());
        Transport motorcycle = JobWithTransport.createInstance("Ямаха", 4);
        System.out.println("Мотоциклы");
        System.out.println();
        try {
            System.out.println("До удаления");
            motorcycle.addModel("Ниндзя", 10);
            System.out.println("цена мотоцикла " + motorcycle.getPriceByName("nameModel0"));
            JobWithTransport.printModels(motorcycle);
            motorcycle.setModelByName("nameModel1", "nameModel3");
//            motorcycle.addModel("Ниндзя", -1.0); //для исключения
//            motorcycle.addModel("nameModel0", 1.0);
            motorcycle.removeModel("Ниндзя", 10);
            System.out.println("После удаления");
            JobWithTransport.printModels(motorcycle);
            System.out.println("Размер " + motorcycle.getSizeModels());
        } catch (DuplicateModelNameException | NoSuchModelNameException e) {
            System.out.println(e.getMessage());
        }

        Motorcycle motorcycleClone = (Motorcycle) motorcycle.clone();
        try {
            motorcycleClone.setPriceByName("nameModel3", 10.0);
            JobWithTransport.printModels(motorcycleClone);
            JobWithTransport.printModels(motorcycle);
        } catch (NoSuchModelNameException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("Среднее ариф-ое: " + JobWithTransport.avgPrice(motorcycle));
    }


}
