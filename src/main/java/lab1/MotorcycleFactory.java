package lab1;

public class MotorcycleFactory implements TransportFactory{
    @Override
    public Transport createTransport(String brand, int sizeModels) {
        return new Motorcycle(brand, sizeModels);
    }
}
