package lab1.exceptions;

public class NoSuchModelNameException extends Exception {
    public NoSuchModelNameException(){
        super("No such model name");
    }
}
