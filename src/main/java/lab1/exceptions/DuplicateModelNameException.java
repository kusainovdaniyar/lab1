package lab1.exceptions;

public class DuplicateModelNameException extends Exception {
    public DuplicateModelNameException(){
        super("Duplicate model name");
    }
}
