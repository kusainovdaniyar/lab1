package lab1.exceptions;

public class FileEmptyException extends Exception {
    public FileEmptyException(){
        super("File is empty!");
    }
}
