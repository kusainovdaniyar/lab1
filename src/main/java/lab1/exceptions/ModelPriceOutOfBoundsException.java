package lab1.exceptions;

public class ModelPriceOutOfBoundsException extends RuntimeException{
    public ModelPriceOutOfBoundsException(){
        super("Model Price Out Of Bounds");
    }
}
