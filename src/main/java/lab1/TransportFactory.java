package lab1;

public interface TransportFactory {
    Transport createTransport(String brand, int sizeModels);
}
