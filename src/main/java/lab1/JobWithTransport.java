package lab1;

public class JobWithTransport {

    private static TransportFactory transportFactory = new CarFactory();

    public static double avgPrice(Transport transport) {
        double[] prices = transport.getPrices();
        double avg = 0;
        for (int i = 0; i < prices.length; i++) {
            avg += prices[i];
        }
        return avg / prices.length;
    }

    public static void printModels(Transport transport) {
        String[] modelNames = transport.getModelsName();
        double[] prices = transport.getPrices();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(transport.getClass());
        stringBuilder.append("\n");
        stringBuilder.append(transport.getBrand());
        stringBuilder.append("\n");
        for (int i = 0; i < modelNames.length; i++) {
            stringBuilder.append(modelNames[i]);
            stringBuilder.append(" ");
            stringBuilder.append(prices[i]);
            stringBuilder.append("\n");
        }
        System.out.println(stringBuilder.toString());
    }

    public static void setTransportFactory(TransportFactory transportFactory) {
        JobWithTransport.transportFactory = transportFactory;
    }

    public static Transport createInstance(String name, int size) {
        return transportFactory.createTransport(name, size);
    }
}
