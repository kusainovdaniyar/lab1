package lab1;

import lab1.exceptions.DuplicateModelNameException;
import lab1.exceptions.NoSuchModelNameException;

public interface Transport extends Cloneable{

    String getBrand();

    void setBrand(String brand);

    String[] getModelsName();

    double getPriceByName(String name) throws NoSuchModelNameException;

    void setPriceByName(String name, double changePrice) throws NoSuchModelNameException;

    double[] getPrices();

    void addModel(String nameModel, double price) throws DuplicateModelNameException;

    void removeModel(String nameModel, double price) throws NoSuchModelNameException;

    int getSizeModels();

    Transport clone() throws CloneNotSupportedException;
    void setModelByName(String name, String changeOn) throws DuplicateModelNameException, NoSuchModelNameException;
}
