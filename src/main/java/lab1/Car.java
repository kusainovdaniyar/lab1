package lab1;

import lab1.exceptions.DuplicateModelNameException;
import lab1.exceptions.ModelPriceOutOfBoundsException;
import lab1.exceptions.NoSuchModelNameException;

import java.util.Arrays;

public class Car implements Transport {
    private String brand;
    private Model[] models;

    public Car(String brand, int sizeModels) {
        this.brand = brand;
        models = new Model[sizeModels];
        for (int i = 0; i < models.length; i++) {
            models[i] = new Model("nameModel" + i, 5 + i);
        }
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String[] getModelsName() {
        String[] modelName = new String[models.length];
        for (int i = 0; i < models.length; i++) {
            modelName[i] = models[i].getName();
        }
        return modelName;
    }

    public double getPriceByName(String name) throws NoSuchModelNameException {
        for (int i = 0; i < models.length; i++) {
            if (models[i].getName().equals(name)) {
                return models[i].getPrice();
            }
        }
        throw new NoSuchModelNameException();
    }

    public void setPriceByName(String name, double changePrice) throws NoSuchModelNameException {
        if (changePrice < 0)
            throw new ModelPriceOutOfBoundsException();
        for (int i = 0; i < models.length; i++) {
            if (models[i].getName().equals(name)) {
                models[i].setPrice(changePrice);
                return;
            }
        }
        throw new NoSuchModelNameException();
    }

    public double[] getPrices() {
        double[] prices = new double[models.length];
        for (int i = 0; i < models.length; i++) {
            prices[i] = models[i].getPrice();
        }
        return prices;
    }

    public void addModel(String nameModel, double price) throws DuplicateModelNameException {
        for (int i = 0; i < models.length; i++) {
            if (models[i].getName().equals(nameModel)) {
                throw new DuplicateModelNameException();
            }
        }
        if (price < 0) {
            throw new ModelPriceOutOfBoundsException();
        }
        models = Arrays.copyOf(models, models.length + 1);
        models[models.length - 1] = new Model(nameModel, price);
    }

    public void removeModel(String nameModel, double price) throws NoSuchModelNameException {
        if (price < 0) {
            throw new ModelPriceOutOfBoundsException();
        }
        for (int i = 0; i < models.length; i++) {
            if (models[i].getName().equals(nameModel) && models[i].getPrice() == price) {
                Model[] newModels = Arrays.copyOf(models, models.length - 1);
                System.arraycopy(models, i + 1, newModels, i, models.length - i - 1);
                models = newModels;
                return;
            }
        }
        throw new NoSuchModelNameException();
    }

    public int getSizeModels() {
        return models.length;
    }

    public void setModelByName(String name, String changeOn) throws DuplicateModelNameException, NoSuchModelNameException {
        for (int i = 0; i <models.length ; i++) {
            if (models[i].getName().equals(changeOn)){
                throw new DuplicateModelNameException();
            }
        }
        for (int i = 0; i < models.length; i++) {
            if (models[i].getName().equals(name)) {
                models[i].setModelName(changeOn);
                return;
            }
        }
        throw new NoSuchModelNameException();
    }

    public Car clone() throws CloneNotSupportedException {
        Car car = (Car) super.clone();
        car.models = Arrays.copyOf(models, models.length);
        for (int i = 0; i < car.models.length; i++) {
            car.models[i] = car.models[i].clone();
        }
        return car;
    }


    private class Model implements Cloneable {
        private String name;
        private double price;

        public Model(String name, double price) {
            this.name = name;
            this.price = price;
        }


        public String getName() {
            return name;
        }

        public void setModelName(String name) {
            this.name = name;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public Model clone() throws CloneNotSupportedException {
            return (Model) super.clone();
        }
    }
}
