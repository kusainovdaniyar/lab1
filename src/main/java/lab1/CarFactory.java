package lab1;

public class CarFactory implements TransportFactory {
    @Override
    public Transport createTransport(String brand, int sizeModels) {
        return new Car(brand, sizeModels);
    }
}
