package lab1;

import lab1.exceptions.FileEmptyException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyLoading {
    private static volatile PropertyLoading instance;
    private final Properties properties;

    private PropertyLoading() {
        properties = new Properties();
        readProperties();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static PropertyLoading getInstance() {
        if (instance == null) {
            synchronized (PropertyLoading.class) {
                if (instance == null) {
                    instance = new PropertyLoading();
                }
            }
        }
        return instance;
    }

    public Properties getProperties() {
        return properties;
    }

    private void readProperties() {
        try {
            String propFileName = "config.properties";
            //относительный путь для пропертей
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
            if (inputStream == null) {
                throw new FileEmptyException();
            }
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (FileEmptyException e) {
            e.printStackTrace();
        }
    }
}
